package com.mariia.view_controller;

import com.mariia.DAO.implementation.ConsumerDAOImpl;
import com.mariia.DAO.implementation.SupermarketDAOImpl;
import com.mariia.DAO.implementation.Supermarket_has_SupplierDAOImpl;
import com.mariia.DAO.implementation.SupplierDAOImpl;
import com.mariia.model.MetaData.Consumer;
import com.mariia.model.MetaData.Supermarket;
import com.mariia.model.MetaData.Supermarket_has_Supplier;
import com.mariia.model.MetaData.Supplier;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MyView {

  private Map<String, String> menu;
  private Map<String, String> menu1;
  private Map<String, String> menu2;
  private Map<String, String> menu3;
  private Map<String, String> menu4;
  private Map<String, Printable> methodsMenu;
  private Map<String, Printable> methodsMenu1;
  private Map<String, Printable> methodsMenu2;
  private Map<String, Printable> methodsMenu3;
  private Map<String, Printable> methodsMenu4;
  private Map<String, String> shortMenu;
  private static Scanner input = new Scanner(System.in);

  public MyView() {
    menu = new LinkedHashMap<>();
    menu1 = new LinkedHashMap<>();
    menu2 = new LinkedHashMap<>();
    menu3 = new LinkedHashMap<>();
    menu4 = new LinkedHashMap<>();
    methodsMenu = new LinkedHashMap<>();
    methodsMenu1 = new LinkedHashMap<>();
    methodsMenu2 = new LinkedHashMap<>();
    methodsMenu3 = new LinkedHashMap<>();
    methodsMenu4 = new LinkedHashMap<>();
    shortMenu = new LinkedHashMap<>();

    shortMenu.put("0","   0 - Select all Tables");
    shortMenu.put("1","   1 - Select table 'Supplier'");
    shortMenu.put("2","   2 - Select table 'Supermarket'");
    shortMenu.put("3","   3 - Select table 'Consumer'");
    shortMenu.put("4","   4 - Select table 'Supermarket_has_Supplier'");
    shortMenu.put("Q", "   Q - Exit");

    menu.put("0", "   0 - Select all Tables");

    menu1.put("1", "   1 - Select all in the table 'Supplier'");
    menu1.put("2", "   2 - Insert to the table 'Supplier'");
    menu1.put("3", "   3 - Delete from the table 'Suplier'");
    menu1.put("4", "   4 - Update for the table 'Supplier'");
    menu1.put("5", "   5 - Find Supplier by id");
    menu1.put("6", "   6 - Find Suplier by name");
    menu1.put("Q", "   Q - Go to upper directory");

    menu2.put("1", "   1 - Find all in the table 'Supermarket");
    menu2.put("2", "   2 - Find Supermarket by id");
    menu2.put("3", "   3 - Find Supermarket by name");
    menu2.put("4", "   4 - Insert to the table 'Supermarket'");
    menu2.put("5", "   5 - Delete from the table 'Supermarket'");
    menu2.put("6", "   6 - Update for the table 'Supermarket'");
    menu2.put("Q", "   Q - Go to upper directory");

    menu3.put("1", "  1 - Find all in the table 'Consumer");
    menu3.put("2", "  2 - Find Consumer by id");
    menu3.put("3", "  3 - Find Consumer by name");
    menu3.put("4", "  4 - Insert to the table 'Consumer'");
    menu3.put("5", "  5 - Delete from the table 'Consumer'");
    menu3.put("6", "  6 - Update for the table 'Consumer'");
    menu3.put("Q", "  Q - Go to upper directory");

    menu4.put("1", "  1 - Find all in the table 'Supermarket_has_Supplier");
    menu4.put("2", "  2 - Insert to the table 'Supermarket_has_Supplier'");
    menu4.put("3", "  3 - Delete from the table 'Supermarket_has_Supplier'");
    menu4.put("4", "  4 - Update for the table 'Supermarket_has_Supplier'");
    menu4.put("Q", "  Q - Go to upper directory");

    menu.put("Q", "   Q - Exit");

    methodsMenu.put("0", MyView::showAllTables);

    methodsMenu1.put("1", MyView::findAll);
    methodsMenu1.put("2", MyView::insertSupplier);
    methodsMenu1.put("3", MyView::deleteSupplier);
    methodsMenu1.put("4", MyView::updateSupplier);
    methodsMenu1.put("5", MyView::findById);
    methodsMenu1.put("6", MyView::findByName);

    methodsMenu2.put("1", MyView::findAllSuperM);
    methodsMenu2.put("2", MyView::findByIdSuperM);
    methodsMenu2.put("3", MyView::findByNameSuperM);
    methodsMenu2.put("4", MyView::insertSupermarket);
    methodsMenu2.put("5", MyView::deleteSupermarket);
    methodsMenu2.put("6", MyView::updateSupermarket);

    methodsMenu3.put("1", MyView::findAllConsumers);
    methodsMenu3.put("2", MyView::findByIdConsumer);
    methodsMenu3.put("3", MyView::findByNameConsumer);
    methodsMenu3.put("4", MyView::insertConsumer);
    methodsMenu3.put("5", MyView::deleteConsumer);
    methodsMenu3.put("6", MyView::updateConsumer);

    methodsMenu4.put("1", MyView::findAllSupermarket_has_Supplier);
    methodsMenu4.put("2", MyView::insertSupermarket_has_Supplier);
    methodsMenu4.put("3", MyView::deleteSupermarket_has_Supplier);
    methodsMenu4.put("4", MyView::updateSupermarket_has_Supplier);
  }

  public static void showAllTables() throws SQLException {
    System.out.println("   SUPPLIER");
    findAll();
    System.out.println("\n   SUPERMARKET");
    findAllSuperM();
    System.out.println("\n   CONSUMER");
    findAllConsumers();
    System.out.println("\n   SUPERMARKET_HAS_SUPPLIER");
    findAllSupermarket_has_Supplier();
  }

  public static void findAll() throws SQLException {
    SupplierDAOImpl supplierDAOImpl = new SupplierDAOImpl();
    List<Supplier> list = supplierDAOImpl.findAll();
    Field fields [] = Supplier.class.getDeclaredFields();
      System.out.println(String.format("%4s %-12s", fields[0].getName(), fields[1].getName()));
    for(Supplier entity : list){
      System.out.println(String.format("%4s %-12s",entity.getId(), entity.getName()));
    }
  }

  public static void insertSupplier() throws SQLException {
    SupplierDAOImpl supplierDAOImpl = new SupplierDAOImpl();
    System.out.println("Please enter id");
    Integer integer = Integer.parseInt(input.next());
    System.out.println("Please enter name");
    String string = input.next();
    supplierDAOImpl.insertSupplier(new Supplier(integer, string));
  }

  public static void deleteSupplier() throws SQLException {
    SupplierDAOImpl supplierDAOImpl = new SupplierDAOImpl();
    System.out.println("Please enter id");
    Integer integer = Integer.parseInt(input.next());
    supplierDAOImpl.deleteSupplier(integer);
  }

  public static void updateSupplier() throws SQLException {
    SupplierDAOImpl supplierDAOImpl = new SupplierDAOImpl();
    System.out.println("Please enter id");
    Integer id = Integer.parseInt(input.next());
    System.out.println("Please enter NEW name");
    String name = input.next();
    supplierDAOImpl.updateSupplier(new Supplier(id, name));
  }

  public static void findById() throws SQLException {
    SupplierDAOImpl supplierDAOImpl = new SupplierDAOImpl();
    System.out.println("Please enter id");
    Integer id = Integer.parseInt(input.next());
    Supplier supplier = supplierDAOImpl.findById(id);
    System.out.println(String.format("%4s %-12s", id, supplier.getName()));
  }

  public static void findByName() throws SQLException {
    SupplierDAOImpl supplierDAOImpl = new SupplierDAOImpl();
    System.out.println("Please enter name");
    String name = input.next();
    Supplier supplier = supplierDAOImpl.findByName(name);
    System.out.println(String.format("%4s %-12s", supplier.getId(), supplier.getName()));
  }
//  ___________________________________________________________________________________________________

  public static void findAllSuperM() throws SQLException {
    SupermarketDAOImpl supermarketDAO = new SupermarketDAOImpl();
    List<Supermarket> list = new ArrayList<>();
    list = supermarketDAO.findAll();
    Field fields [] = Supermarket.class.getDeclaredFields();
    System.out.println(String.format("%4s %-5s %-5s %-12s", fields[0].getName(), fields[2].getName(), fields[3].getName(), fields[1].getName()));
    for(Supermarket entity: list){
      System.out.println(String.format("%4s %tR %tR %-12s", entity.getId(), entity.getOpen(), entity.getClosed(), entity.getName()));
    }
  }

  public static void findByIdSuperM() throws SQLException {
    SupermarketDAOImpl supermarketDAO = new SupermarketDAOImpl();
    System.out.println("Please enter id");
    Integer id = Integer.parseInt(input.next());
    Supermarket supermarket = supermarketDAO.findById(id);
    System.out.println(String.format("%4s %tR %tR %-12s", supermarket.getId(), supermarket.getOpen(), supermarket.getClosed(), supermarket.getName()));
  }

  public static void findByNameSuperM() throws SQLException {
    SupermarketDAOImpl supermarketDAO = new SupermarketDAOImpl();
    System.out.println("Please enter name");
    String name = input.next();
    Supermarket supermarket = supermarketDAO.findByName(name);
    System.out.println(String.format("%4s %tR %tR %-12s", supermarket.getId(), supermarket.getOpen(), supermarket.getClosed(), supermarket.getName()));
  }

  public static void insertSupermarket() throws SQLException {
    SupermarketDAOImpl supermarketDAO = new SupermarketDAOImpl();
    System.out.println("Please enter id");
    Integer integer = Integer.parseInt(input.next());
    System.out.println("Please enter time of 'Open'");
    Time open = Time.valueOf(input.next());
    System.out.println("Please enter time of 'Close'");
    Time close = Time.valueOf(input.next());
    System.out.println("Please enter name");
    String string = input.next();
    supermarketDAO.insertSupermarket(new Supermarket(integer, open, close, string));
  }

  public static void deleteSupermarket() throws SQLException {
    SupermarketDAOImpl supermarketDAO = new SupermarketDAOImpl();
    System.out.println("Please enter id");
    Integer integer = Integer.parseInt(input.next());
    supermarketDAO.deleteSupermarket(integer);
  }

  public static void updateSupermarket() throws SQLException {
    SupermarketDAOImpl supermarketDAO = new SupermarketDAOImpl();
    System.out.println("Please enter id");
    Integer id = Integer.parseInt(input.next());
    System.out.println("Please enter NEW time of 'Open'");
    Time open = Time.valueOf(input.next());
    System.out.println("Please enter NEW time of 'Close'");
    Time close = Time.valueOf(input.next());
    System.out.println("Please enter NEW name");
    String name = input.next();
    supermarketDAO.updateSupermarket(new Supermarket(id, open, close, name));
  }

  //___________________________________________________________________________________________________

  public static void findAllConsumers() throws SQLException {
    ConsumerDAOImpl consumerDAO = new ConsumerDAOImpl();
    List<Consumer> list = consumerDAO.findAll();
    Field fields [] = Consumer.class.getDeclaredFields();
    System.out.println(String.format("%4s %-12s %-4s", fields[0].getName(), fields[1].getName(), fields[2].getName()));
    for(Consumer entity : list){
      System.out.println(String.format("%4s %-12s %4s",entity.getId(), entity.getName(), entity.getSupermarket_id()));
    }
  }

  public static void insertConsumer() throws SQLException {
    ConsumerDAOImpl consumerDAO = new ConsumerDAOImpl();
    System.out.println("Please enter id");
    Integer integer = Integer.parseInt(input.next());
    System.out.println("Please enter name");
    String string = input.next();
    System.out.println("Please enter Supermarket id");
    Integer superm = Integer.parseInt(input.next());
    consumerDAO.insertConsumer(new Consumer(integer, string, superm));
  }

  public static void deleteConsumer() throws SQLException {
    ConsumerDAOImpl consumerDAO = new ConsumerDAOImpl();
    System.out.println("Please enter id");
    Integer integer = Integer.parseInt(input.next());
    consumerDAO.deleteConsumer(integer);
  }

  public static void updateConsumer() throws SQLException {
    ConsumerDAOImpl consumerDAO = new ConsumerDAOImpl();
    System.out.println("Please enter id");
    Integer id = Integer.parseInt(input.next());
    System.out.println("Please enter NEW name");
    String name = input.next();
    System.out.println("Please enter NEW Supermarket id");
    Integer superm = Integer.parseInt(input.next());
    consumerDAO.updateConsumer(new Consumer(id, name, superm));
  }

  public static void findByIdConsumer() throws SQLException {
    ConsumerDAOImpl consumerDAO = new ConsumerDAOImpl();
    System.out.println("Please enter id");
    Integer id = Integer.parseInt(input.next());
    Consumer consumer = consumerDAO.findById(id);
    System.out.println(String.format("%4s %-12s %-4s", id, consumer.getName(), consumer.getSupermarket_id()));
  }

  public static void findByNameConsumer() throws SQLException {
    ConsumerDAOImpl consumerDAO = new ConsumerDAOImpl();
    System.out.println("Please enter name");
    String name = input.next();
    Consumer consumer = consumerDAO.findByName(name);
    System.out.println(String.format("%4s %-12s %-4s", consumer.getId(), consumer.getName(), consumer.getSupermarket_id()));
  }

  //___________________________________________________________________________________________________

  public static void findAllSupermarket_has_Supplier() throws SQLException {
    Supermarket_has_SupplierDAOImpl supermarket_has_supplierDAO = new Supermarket_has_SupplierDAOImpl();
    List<Supermarket_has_Supplier> list = supermarket_has_supplierDAO.findAll();
    Field fields [] = Supermarket_has_Supplier.class.getDeclaredFields();
    System.out.println(String.format("%1s %12s %12s", "", fields[0].getName(), fields[1].getName()));
    for(Supermarket_has_Supplier entity : list){
      System.out.println(String.format("%12s %12s", entity.getSupermarket_id(), entity.getSupplier_id()));
    }
  }

  public static void insertSupermarket_has_Supplier() throws SQLException {
    Supermarket_has_SupplierDAOImpl supermarket_has_supplierDAO = new Supermarket_has_SupplierDAOImpl();
    System.out.println("Please enter supermarket id");
    Integer id1 = Integer.parseInt(input.next());
    System.out.println("Please enter supplier id");
    Integer id2 = Integer.parseInt(input.next());
    supermarket_has_supplierDAO.insertSupermarket_has_Supplier(new Supermarket_has_Supplier(id1, id2));
  }

  public static void deleteSupermarket_has_Supplier() throws SQLException {
    Supermarket_has_SupplierDAOImpl supermarket_has_supplierDAO = new Supermarket_has_SupplierDAOImpl();
    System.out.println("Please enter supermarket id");
    Integer id1 = Integer.parseInt(input.next());
    System.out.println("Please enter supplier id");
    Integer id2 = Integer.parseInt(input.next());
    supermarket_has_supplierDAO.deleteSupermarket_has_Supplier(id1, id2);
  }

  public static void updateSupermarket_has_Supplier() throws SQLException {
    Supermarket_has_SupplierDAOImpl supermarket_has_supplierDAO = new Supermarket_has_SupplierDAOImpl();
    System.out.println("Please enter supermarket id");
    Integer id1 = Integer.parseInt(input.next());
    System.out.println("Please enter supplier id");
    Integer id2 = Integer.parseInt(input.next());
    System.out.println("Please enter NEW supplier_id");
    Integer id3 = Integer.parseInt(input.next());
    supermarket_has_supplierDAO.updateSupermarket_has_Supplier(new Supermarket_has_Supplier(id1, id2), id3);
  }



  public void print() {

    System.out.println("   MENU:");
    for (Map.Entry map : shortMenu.entrySet()) {
      System.out.println(map.getValue());
    }
  }

  public void printSub() {
    print();
    String str1 = input.next();
    while (!str1.equals("Q")) {
      if(str1.equals("0")){
        System.out.println("   Please wait ... \n");
        try {
          methodsMenu.get(str1).print();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
      else if (str1.equals("1")) {
        for (Map.Entry map : menu1.entrySet()) {
          System.out.println(map.getValue());
        }
        String str2 = input.next();
        while (!str2.equals("Q")) {
          if (methodsMenu1.containsKey(str2)) {

            System.out.println("   Please wait ... \n");
            try {
              methodsMenu1.get(str2).print();
            } catch (SQLException e) {
              e.printStackTrace();
            }
            System.out.println(" ");
            for (Map.Entry map : menu1.entrySet()) {
              System.out.println(map.getValue());
            }
            str2 = input.next();
          } else {
            System.out.println("Please enter a correct number");
            str2 = input.next();
          }
        }
      }

      else if (str1.equals("2")) {
        for (Map.Entry map : menu2.entrySet()) {
          System.out.println(map.getValue());
        }
        String str2 = input.next();
        while (!str2.equals("Q")) {
          if (methodsMenu2.containsKey(str2)) {

            System.out.println("   Please wait ... \n");
            try {
              methodsMenu2.get(str2).print();
            } catch (SQLException e) {
              e.printStackTrace();
            }
            System.out.println(" ");
            for (Map.Entry map : menu2.entrySet()) {
              System.out.println(map.getValue());
            }
            str2 = input.next();
          } else {
            System.out.println("Please enter a correct number");
            str2 = input.next();
          }
        }
      } else if (str1.equals("3")) {
        for (Map.Entry map : menu3.entrySet()) {
          System.out.println(map.getValue());
        }
        String str2 = input.next();
        while (!str2.equals("Q")) {
          if (methodsMenu3.containsKey(str2)) {

            System.out.println("   Please wait ... \n");
            try {
              methodsMenu3.get(str2).print();
            } catch (SQLException e) {
              e.printStackTrace();
            }
            System.out.println(" ");
            for (Map.Entry map : menu3.entrySet()) {
              System.out.println(map.getValue());
            }
            str2 = input.next();
          } else {
            System.out.println("Please enter a correct number");
            str2 = input.next();
          }
        }
      } else if (str1.equals("4")) {
        for (Map.Entry map : menu4.entrySet()) {
          System.out.println(map.getValue());
        }
        String str2 = input.next();
        while (!str2.equals("Q")) {
          if (methodsMenu4.containsKey(str2)) {

            System.out.println("   Please wait ... \n");
            try {
              methodsMenu4.get(str2).print();
            } catch (SQLException e) {
              e.printStackTrace();
            }
            System.out.println(" ");
            for (Map.Entry map : menu4.entrySet()) {
              System.out.println(map.getValue());
            }
            str2 = input.next();
          } else {
            System.out.println("Please enter a correct number");
            str2 = input.next();
          }
        }
      } else {
        System.out.println("Please enter a correct number\n");
      }
      print();
      str1 = input.next();
    }
  }

  public static void main(String[] args) {
    MyView myView = new MyView();
    myView.printSub();



  }

}
