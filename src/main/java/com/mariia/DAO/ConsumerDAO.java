package com.mariia.DAO;

import com.mariia.model.MetaData.Consumer;
import java.sql.SQLException;
import java.util.List;

public interface ConsumerDAO {

  List<Consumer> findAll() throws SQLException;
  Consumer findByName(String name) throws SQLException;
  Consumer findById(Integer id) throws SQLException;

  int insertConsumer(Consumer consumer) throws SQLException;
  int updateConsumer(Consumer consumer) throws SQLException;
  int deleteConsumer(Integer id) throws SQLException;

}
