package com.mariia.DAO;

import com.mariia.model.MetaData.Supermarket_has_Supplier;
import java.sql.SQLException;
import java.util.List;

public interface Supermarket_has_SupplierDAO {

  List<Supermarket_has_Supplier> findAll() throws SQLException;

  int insertSupermarket_has_Supplier(Supermarket_has_Supplier supermarket_has_supplier) throws SQLException;
  int updateSupermarket_has_Supplier(Supermarket_has_Supplier supermarket_has_supplier, Integer id) throws SQLException;
  int deleteSupermarket_has_Supplier(Integer id1, Integer id2) throws SQLException;

}
