package com.mariia.DAO;

import com.mariia.model.MetaData.Supermarket;
import java.sql.SQLException;
import java.util.List;

public interface SupermarketDAO {

List<Supermarket> findAll() throws SQLException;
Supermarket findById(int id) throws SQLException;
Supermarket findByName(String name) throws SQLException;

int insertSupermarket(Supermarket supermarket) throws SQLException;
int deleteSupermarket(Integer id) throws SQLException;
int updateSupermarket(Supermarket supermarket) throws SQLException;



}
