package com.mariia.DAO;

import com.mariia.model.MetaData.Supplier;
import java.sql.SQLException;
import java.util.List;

public interface SupplierDAO {

  List<Supplier> findAll() throws SQLException;
  Supplier findByName(String name) throws SQLException;
  Supplier findById(Integer id) throws SQLException;

  int insertSupplier(Supplier supplier) throws SQLException;
  int updateSupplier(Supplier supplier) throws SQLException;
  int deleteSupplier(Integer id) throws SQLException;

}
