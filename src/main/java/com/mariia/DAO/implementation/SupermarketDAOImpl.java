package com.mariia.DAO.implementation;

import com.mariia.DAO.SupermarketDAO;
import com.mariia.model.MetaData.Supermarket;
import com.mariia.persistence.ConnectionManager;
import com.mariia.transformer.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

public class SupermarketDAOImpl implements SupermarketDAO {

  private static final String FIND_ALL = "SELECT * FROM supermarket";
  private static final String FINDBYID = "SELECT * FROM supermarket WHERE id = ?";
  private static final String FINDBYNAME = "SELECT * FROM supermarket WHERE name = ?";
  private static final String INSERT = "INSERT INTO supermarket (id, open, closed, name) VALUES (?, ?, ?, ?)";
  private static final String UPDATE = "UPDATE supermarket SET id=?, open=?, closed=?, name=? WHERE id= ?";
  private static final String DELETE = "DELETE FROM supermarket WHERE id =?";

  @Override
  public List<Supermarket> findAll() throws SQLException {
    List<Supermarket> supermarkets = new ArrayList<>();
    Connection connection = ConnectionManager.getConnection();
      try(Statement statement = connection.createStatement()){
        try(ResultSet resultSet =statement.executeQuery(FIND_ALL)){
          while (resultSet.next()){
          supermarkets.add(
              (Supermarket)new Transformer(Supermarket.class).fromResultSetToEntity(resultSet));
          }
        }
      }
    return supermarkets;
  }

  @Override
  public Supermarket findById(int id) throws SQLException {
    Supermarket supermarket = new Supermarket();
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(FINDBYID)){
      preparedStatement.setInt(1, id);
      ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
          supermarket = (Supermarket) new Transformer(Supermarket.class).fromResultSetToEntity(resultSet);
          break;
        }
      return supermarket;
    }
  }

  @Override
  public Supermarket findByName(String name) throws SQLException {
    Supermarket supermarket = new Supermarket();
    Connection connection = ConnectionManager.getConnection();
      try (PreparedStatement preparedStatement = connection.prepareStatement(FINDBYNAME)){
        preparedStatement.setString(1, name);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()){
          supermarket = (Supermarket) new Transformer(Supermarket.class).fromResultSetToEntity(resultSet);
          break;
        }
        return supermarket;
      }
  }

  @Override
  public int insertSupermarket(Supermarket supermarket) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT)) {
      preparedStatement.setInt(1, supermarket.getId());
      preparedStatement.setTime(2, supermarket.getOpen());
      preparedStatement.setTime(3, supermarket.getClosed());
      preparedStatement.setString(4, supermarket.getName());
      return preparedStatement.executeUpdate();
    }
  }

  @Override
  public int deleteSupermarket(Integer id) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try(PreparedStatement preparedStatement = connection.prepareStatement(DELETE)){
    preparedStatement.setInt(1, id);
    return preparedStatement.executeUpdate();
    }
  }

  @Override
  public int updateSupermarket(Supermarket supermarket) throws SQLException {
      Connection connection = ConnectionManager.getConnection();
      try(PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)){
        preparedStatement.setInt(1, supermarket.getId());
        preparedStatement.setTime(2, supermarket.getOpen());
        preparedStatement.setTime(3, supermarket.getClosed());
        preparedStatement.setString(4, supermarket.getName());
        preparedStatement.setInt(5, supermarket.getId());
      return preparedStatement.executeUpdate();
    }
  }

}
