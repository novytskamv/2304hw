package com.mariia.DAO.implementation;

import com.mariia.DAO.Supermarket_has_SupplierDAO;
import com.mariia.model.MetaData.Supermarket_has_Supplier;
import com.mariia.model.MetaData.Supplier;
import com.mariia.persistence.ConnectionManager;
import com.mariia.transformer.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Supermarket_has_SupplierDAOImpl implements Supermarket_has_SupplierDAO {

  private static final String FIND_ALL = "SELECT * FROM supermarket_has_supplier";
  private static final String INSERT_SUPERMARKET_HAS_SUPPLIER = "INSERT INTO supermarket_has_supplier(supermarket_id, supplier_id) VALUES(?,?)";
  private static final String DELETE_SUPERMARKET_HAS_SUPPLIER = "DELETE FROM supermarket_has_supplier WHERE supermarket_id=? AND supplier_id=?";
  private static final String UPDATE_SUPERMARKET_HAS_SUPPLIER = "UPDATE supermarket_has_supplier SET supplier_id=? WHERE supermarket_id=? AND supplier_id=?";

  @Override
  public List<Supermarket_has_Supplier> findAll() throws SQLException {
    List<Supermarket_has_Supplier> supermarket_has_suppliers = new ArrayList<>();
    Connection connection = ConnectionManager.getConnection();
    try(Statement statement = connection.createStatement()) {
      try(ResultSet resultSet= statement.executeQuery(FIND_ALL)){
        while (resultSet.next()){
          supermarket_has_suppliers.add((Supermarket_has_Supplier) new Transformer(Supermarket_has_Supplier.class).fromResultSetToEntity(resultSet));
        }
      }
    }
    return supermarket_has_suppliers;
  }


  @Override
  public int insertSupermarket_has_Supplier(Supermarket_has_Supplier supermarket_has_supplier)
      throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try(PreparedStatement preparedStatement = connection.prepareStatement(INSERT_SUPERMARKET_HAS_SUPPLIER)){
      preparedStatement.setInt(1, supermarket_has_supplier.getSupermarket_id());
      preparedStatement.setInt(2, supermarket_has_supplier.getSupplier_id());
      return preparedStatement.executeUpdate();
    }
  }

  @Override
  public int updateSupermarket_has_Supplier(Supermarket_has_Supplier supermarket_has_supplier, Integer id)
      throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try(PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SUPERMARKET_HAS_SUPPLIER)){
      preparedStatement.setInt(3, supermarket_has_supplier.getSupplier_id());
      preparedStatement.setInt(2, supermarket_has_supplier.getSupermarket_id());
      supermarket_has_supplier.setSupplier_id(id);
      preparedStatement.setInt(1, supermarket_has_supplier.getSupplier_id());
      return preparedStatement.executeUpdate();
    }
  }

  @Override
  public int deleteSupermarket_has_Supplier(Integer id1, Integer id2) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try(PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SUPERMARKET_HAS_SUPPLIER)){
      preparedStatement.setInt(1, id1);
      preparedStatement.setInt(2, id2);
      return preparedStatement.executeUpdate();
    }
  }
}
