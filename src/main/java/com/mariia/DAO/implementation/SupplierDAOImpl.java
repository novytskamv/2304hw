package com.mariia.DAO.implementation;

import com.mariia.DAO.SupplierDAO;
import com.mariia.model.MetaData.Supplier;
import com.mariia.persistence.ConnectionManager;
import com.mariia.transformer.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class SupplierDAOImpl implements SupplierDAO {

  private static final String FIND_ALL = "SELECT * FROM supplier";
  private static final String FIND_BY_ID = "SELECT * FROM supplier WHERE id=?";
  private static final String FIND_BY_NAME = "SELECT * FROM supplier WHERE mame=?";
  private static final String INSERT_SUPPLIER = "INSERT INTO supplier(id, mame) VALUES(?,?)";
  private static final String DELETE_SUPPLIER = "DELETE FROM supplier WHERE id=?";
  private static final String UPDATE_SUPPLIER = "UPDATE supplier SET id =?, mame=? WHERE id=?";


  @Override
  public List<Supplier> findAll() throws SQLException {
    List<Supplier> suppliers = new ArrayList<>();
    Connection connection = ConnectionManager.getConnection();
    try(Statement statement = connection.createStatement()) {
      try(ResultSet resultSet= statement.executeQuery(FIND_ALL)){
      while (resultSet.next()){
        suppliers.add((Supplier) new Transformer(Supplier.class).fromResultSetToEntity(resultSet));
        }
      }
    }
    return suppliers;
  }

  @Override
  public Supplier findByName(String name) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try(PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_NAME)){
      preparedStatement.setString(1, name);
      try(ResultSet resultSet = preparedStatement.executeQuery()){
        while (resultSet.next()){
          Supplier supplier = (Supplier) new Transformer(Supplier.class).fromResultSetToEntity(resultSet);
          return supplier;
        }
      }
    }
    return null;
  }

  @Override
  public Supplier findById(Integer id) throws SQLException {

    Connection connection = ConnectionManager.getConnection();
    try(PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID)){
      preparedStatement.setInt(1, id);
      try(ResultSet resultSet = preparedStatement.executeQuery()) {
        while (resultSet.next()){
          Supplier supplier = (Supplier) new Transformer(Supplier.class).fromResultSetToEntity(resultSet);
          return supplier;
        }
      }
    }
    return null;
  }

  @Override
  public int insertSupplier(Supplier supplier) throws SQLException {

    Connection connection = ConnectionManager.getConnection();
    try(PreparedStatement preparedStatement = connection.prepareStatement(INSERT_SUPPLIER)){
      preparedStatement.setInt(1, supplier.getId());
      preparedStatement.setString(2, supplier.getName());
      return preparedStatement.executeUpdate();
    }
  }

  @Override
  public int updateSupplier(Supplier supplier) throws SQLException {

    Connection connection = ConnectionManager.getConnection();
    try(PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SUPPLIER)){
      preparedStatement.setInt(1, supplier.getId());
      preparedStatement.setString(2, supplier.getName());
      preparedStatement.setInt(3, supplier.getId());
      return preparedStatement.executeUpdate();
    }
  }

  @Override
  public int deleteSupplier(Integer id) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try(PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SUPPLIER)){
      preparedStatement.setInt(1, id);
      return preparedStatement.executeUpdate();
    }
  }



}
