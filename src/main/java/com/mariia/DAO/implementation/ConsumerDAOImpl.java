package com.mariia.DAO.implementation;

import com.mariia.DAO.ConsumerDAO;
import com.mariia.model.MetaData.Consumer;
import com.mariia.model.MetaData.Supplier;
import com.mariia.persistence.ConnectionManager;
import com.mariia.transformer.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ConsumerDAOImpl implements ConsumerDAO {

  private static final String FIND_ALL = "SELECT * FROM consumer";
  private static final String FIND_BY_ID = "SELECT * FROM consumer WHERE id=?";
  private static final String FIND_BY_NAME = "SELECT * FROM consumer WHERE name=?";
  private static final String INSERT_CONSUMER = "INSERT INTO consumer(id, name, supermarket_id) VALUES(?,?,?)";
  private static final String DELETE_CONSUMER = "DELETE FROM consumer WHERE id=?";
  private static final String UPDATE_CONSUMER = "UPDATE consumer SET id =?, name=?, supermarket_id=? WHERE id=?";

  @Override
  public List<Consumer> findAll() throws SQLException {
    List<Consumer> consumers = new ArrayList<>();
    Connection connection = ConnectionManager.getConnection();
    try(Statement statement = connection.createStatement()) {
      try(ResultSet resultSet= statement.executeQuery(FIND_ALL)){
        while (resultSet.next()){
          consumers.add((Consumer) new Transformer(Consumer.class).fromResultSetToEntity(resultSet));
        }
      }
    }
    return consumers;
  }

  @Override
  public Consumer findByName(String name) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try(PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_NAME)){
      preparedStatement.setString(1, name);
      try(ResultSet resultSet = preparedStatement.executeQuery()){
        while (resultSet.next()){
          Consumer consumer = (Consumer) new Transformer(Consumer.class).fromResultSetToEntity(resultSet);
          return consumer;
        }
      }
    }
    return null;
  }

  @Override
  public Consumer findById(Integer id) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try(PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID)){
      preparedStatement.setInt(1, id);
      try(ResultSet resultSet = preparedStatement.executeQuery()) {
        while (resultSet.next()){
          Consumer consumer = (Consumer) new Transformer(Consumer.class).fromResultSetToEntity(resultSet);
          return consumer;
        }
      }
    }
    return null;
  }

  @Override
  public int insertConsumer(Consumer consumer) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try(PreparedStatement preparedStatement = connection.prepareStatement(INSERT_CONSUMER)){
      preparedStatement.setInt(1, consumer.getId());
      preparedStatement.setString(2, consumer.getName());
      preparedStatement.setInt(3, consumer.getSupermarket_id());
      return preparedStatement.executeUpdate();
    }
  }

  @Override
  public int updateConsumer(Consumer consumer) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try(PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_CONSUMER)){
      preparedStatement.setInt(1, consumer.getId());
      preparedStatement.setString(2, consumer.getName());
      preparedStatement.setInt(3, consumer.getSupermarket_id());
      preparedStatement.setInt(4, consumer.getId());
      return preparedStatement.executeUpdate();
    }
  }

  @Override
  public int deleteConsumer(Integer id) throws SQLException {
    Connection connection = ConnectionManager.getConnection();
    try(PreparedStatement preparedStatement = connection.prepareStatement(DELETE_CONSUMER)){
      preparedStatement.setInt(1, id);
      return preparedStatement.executeUpdate();
    }
  }
}
