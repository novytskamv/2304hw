package com.mariia.model.MetaData;

import com.mariia.model.Annotation.Column;
import com.mariia.model.Annotation.Table;

@Table(name = "supermarket_has_supplier")
public class Supermarket_has_Supplier {

  @Column(name = "supermarket_id")
  private Integer supermarket_id;
  @Column(name = "supplier_id")
  private Integer supplier_id;

  public Supermarket_has_Supplier(Integer supermarket_id, Integer supplier_id) {
    this.supermarket_id = supermarket_id;
    this.supplier_id = supplier_id;
  }

  public Supermarket_has_Supplier() {
  }

  public Integer getSupermarket_id() {
    return supermarket_id;
  }

  public void setSupermarket_id(Integer supermarket_id) {
    this.supermarket_id = supermarket_id;
  }

  public Integer getSupplier_id() {
    return supplier_id;
  }

  public void setSupplier_id(Integer supplier_id) {
    this.supplier_id = supplier_id;
  }
}
