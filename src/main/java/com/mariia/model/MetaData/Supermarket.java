package com.mariia.model.MetaData;

import com.mariia.model.Annotation.Column;
import com.mariia.model.Annotation.PrimaryKey;
import com.mariia.model.Annotation.Table;
import java.sql.Time;

@Table(name = "supermarket")
public class Supermarket {

  @PrimaryKey
  @Column(name = "id")
  private Integer id;

  @Column(name = "name")
  private String name;

  @Column(name = "open")
  private Time open;

  @Column(name = "closed")
  private Time closed;

  public Supermarket(Integer id, Time open, Time closed, String name) {
    this.id = id;
    this.open = open;
    this.closed = closed;
    this.name = name;
  }

  public Supermarket() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Time getOpen() {
    return open;
  }

  public void setOpen(Time open) {
    this.open = open;
  }

  public Time getClosed() {
    return closed;
  }

  public void setClosed(Time closed) {
    this.closed = closed;
  }

  @Override
  public String toString() {
    return "Supermarket{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", open=" + open +
        ", closed=" + closed +
        '}';
  }
}
