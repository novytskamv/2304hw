package com.mariia.model.MetaData;

import com.mariia.model.Annotation.Column;
import com.mariia.model.Annotation.PrimaryKey;
import com.mariia.model.Annotation.Table;

@Table(name = "consumer")
public class Consumer {

  @PrimaryKey
  @Column(name = "id")
  private Integer id;
  @Column(name = "name")
  private String name;
  @Column(name = "supermarket_id")
  private Integer supermarket_id;

  public Consumer(Integer id, String name, Integer supermarket_id) {
    this.id = id;
    this.name = name;
    this.supermarket_id = supermarket_id;
  }

  public Consumer() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getSupermarket_id() {
    return supermarket_id;
  }

  public void setSupermarket_id(Integer supermarket_id) {
    this.supermarket_id = supermarket_id;
  }
}
