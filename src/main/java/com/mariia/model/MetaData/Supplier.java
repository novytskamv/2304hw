package com.mariia.model.MetaData;

import com.mariia.model.Annotation.Column;
import com.mariia.model.Annotation.PrimaryKey;
import com.mariia.model.Annotation.Table;

@Table(name = "supplier")
public class Supplier {
  @PrimaryKey
  @Column(name = "id")
  private Integer id;
  @Column(name = "mame")
  private String mame;

  public Supplier(Integer id, String mame) {
    this.id = id;
    this.mame = mame;
  }

  public Supplier() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return mame;
  }

  public void setName(String name) {
    this.mame = name;
  }

}
