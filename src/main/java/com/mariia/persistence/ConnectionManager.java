package com.mariia.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

  private static final String url ="jdbc:mysql://localhost:3306/supermarket?autoReconnect=true&serverTimezone=UTC&useSSL=false&autoReconnect=true&useUnicode=yes&characterEncoding=UTF-8";
  private static final String user = "root";
  private static final String password = "";

  private static Connection connection = null;

  public ConnectionManager() {
  }

  public static Connection getConnection(){
    if(connection == null)
    {
      try {
        connection = DriverManager.getConnection(url, user, password);
      } catch (SQLException e) {
        System.out.println("Connection generates an SQLException");
      }
    }
    return connection;
  }
}
