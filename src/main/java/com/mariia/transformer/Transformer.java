package com.mariia.transformer;

import com.mariia.model.Annotation.Column;
import com.mariia.model.Annotation.PrimaryKey;
import com.mariia.model.Annotation.Table;
import com.mariia.model.MetaData.Supplier;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;

public class Transformer <T>{

  private final Class<T> tClass;

  public Transformer(Class<T> tClass) {
    this.tClass = tClass;
  }


  public Object fromResultSetToEntity(ResultSet resultSet){

    Object entity = null;

    try {
      entity = tClass.getConstructor().newInstance();
      if(tClass.isAnnotationPresent(Table.class)){
        Field fields [] = tClass.getDeclaredFields();
        for(Field field:fields){
          if(field.isAnnotationPresent(Column.class)){
            Column column = field.getAnnotation(Column.class);
            String name = column.name();
            field.setAccessible(true);
            Class type = field.getType();
            if(type==String.class){
              field.set(entity, resultSet.getString(name));
            }else if(type==Integer.class){
              field.set(entity, resultSet.getInt(name));
            }else if(type==Date.class){
              field.set(entity, resultSet.getDate(name));
            }else if(type==Time.class){
              field.set(entity, resultSet.getTime(name));
            }
          }
          else if(field.isAnnotationPresent(PrimaryKey.class)){
            field.setAccessible(true);
            Class type = field.getType();
            Object FK = type.getConstructor().newInstance();
            field.set(entity, FK);
            Field[] fieldsInner = type.getDeclaredFields();
            for (Field fieldInner : fieldsInner){
              if (fieldInner.isAnnotationPresent(Column.class)){
                Column column = (Column) fieldInner.getAnnotation(Column.class);
                String name = column.name();
                fieldInner.setAccessible(true);
                Class fieldInnerType = fieldInner.getType();
                if (fieldInnerType == String.class) {
                  fieldInner.set(FK, resultSet.getString(name));
                } else if (fieldInnerType == Integer.class) {
                  fieldInner.set(FK, resultSet.getInt(name));
                } else if (fieldInnerType == Date.class) {
                  fieldInner.set(FK, resultSet.getDate(name));
                } else if (fieldInnerType==Time.class){
                  fieldInner.set(FK, resultSet.getTime(name));
                }
              }
            }
          }
        }
      }

    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }  catch (SQLException e) {
      e.printStackTrace();
    } catch (NoSuchMethodException e) {
      e.printStackTrace();
    } catch (InvocationTargetException e) {
      e.printStackTrace();
    }
    return entity;
  }
}
